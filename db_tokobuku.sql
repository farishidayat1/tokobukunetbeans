-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2017 at 07:11 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tokobuku`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_barang`
--

CREATE TABLE `tabel_barang` (
  `kode` varchar(15) NOT NULL,
  `nama_sup` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jenis` varchar(50) NOT NULL,
  `satuan` varchar(25) NOT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_barang`
--

INSERT INTO `tabel_barang` (`kode`, `nama_sup`, `nama`, `jenis`, `satuan`, `harga`, `jumlah`) VALUES
('0001', 'PT. Pelajaran', 'Bahasa Inggris', 'Pelajaran', 'Pcs', 19000, 1),
('0002', 'PT. Pelajaran', 'Sejarah', 'Pelajaran', 'Eksemplar', 100000, 1),
('0003', 'PT. Majalah', 'Bobo', 'Majalah', 'Eksemplar', 1000000, 10),
('1231', 'PT. Pelajaran', 'coba dulu', 'Novel', 'Pcs', 14000, 12),
('1234', 'PT. Pelajaran', 'matahari ku', 'Majalah', 'Eksemplar', 12000, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_detail_penjualan`
--

CREATE TABLE `tabel_detail_penjualan` (
  `no_transaksi` varchar(15) NOT NULL,
  `kode_brg` varchar(15) NOT NULL,
  `tanggal` date NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(10) UNSIGNED NOT NULL,
  `jumlah` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_detail_penjualan`
--

INSERT INTO `tabel_detail_penjualan` (`no_transaksi`, `kode_brg`, `tanggal`, `nama`, `harga`, `jumlah`, `total`) VALUES
('TI.PJ.435.1', '0001', '2016-05-31', 'Bahasa Inggris', 19000, 1, 19000),
('TI.PJ.435.13', '0002', '2016-05-31', 'Sejarah', 100000, 1, 100000),
('TI.PJ.435.14', '0002', '2016-05-31', 'Sejarah', 100000, 1, 100000),
('TI.PJ.435.15', '0001', '2016-05-31', 'Bahasa Inggris', 19000, 2, 38000),
('TI.PJ.435.16', '0001', '2016-05-31', 'Bahasa Inggris', 19000, 1, 19000),
('TI.PJ.435.19', '0001', '2016-06-01', 'Bahasa Inggris', 19000, 2, 38000),
('TI.PJ.435.20', '0003', '2016-06-01', 'Biografi Soekarno', 80000, 1, 80000),
('TI.PJ.435.21', '0003', '2016-06-01', 'Biografi Soekarno', 80000, 1, 80000),
('TI.PJ.435.22', '0003', '2016-06-01', 'Biografi Soekarno', 80000, 1, 80000),
('TI.PJ.435.23', '0002', '2016-06-02', 'Sejarah', 100000, 1, 100000),
('TI.PJ.435.24', '0001', '2016-06-03', 'Bahasa Inggris', 19000, 2, 38000),
('TI.PJ.435.25', '1234', '2016-06-13', 'matahari ku', 12000, 20, 240000),
('TI.PJ.435.26', '0003', '2017-03-19', 'Bobo', 1000000, 2, 2000000),
('TI.PJ.435.27', '0002', '2017-03-19', 'Sejarah', 100000, 2, 200000);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_karyawan`
--

CREATE TABLE `tabel_karyawan` (
  `kode` varchar(15) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kelamin` varchar(15) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telp` varchar(18) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_karyawan`
--

INSERT INTO `tabel_karyawan` (`kode`, `nama`, `kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `telp`) VALUES
('0001', 'Reza Yudhistira', 'Laki-laki', 'Depok', '1999-11-25', 'Depoks', '080908090809');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_penjualan`
--

CREATE TABLE `tabel_penjualan` (
  `no` int(10) UNSIGNED NOT NULL,
  `no_transaksi` varchar(15) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL,
  `total_belanja` int(10) UNSIGNED NOT NULL,
  `uang_bayar` int(10) UNSIGNED NOT NULL,
  `uang_kembali` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_penjualan`
--

INSERT INTO `tabel_penjualan` (`no`, `no_transaksi`, `tanggal`, `waktu`, `total_belanja`, `uang_bayar`, `uang_kembali`) VALUES
(12, 'TI.PJ.435.1', '2016-05-31', '11:15:31', 19000, 20000, 1000),
(13, 'TI.PJ.435.13', '2016-05-31', '16:20:30', 100000, 100000, 0),
(14, 'TI.PJ.435.14', '2016-05-31', '16:21:55', 100000, 100000, 0),
(15, 'TI.PJ.435.15', '2016-05-31', '16:39:47', 38000, 40000, 2000),
(18, 'TI.PJ.435.16', '2016-05-31', '16:46:29', 19000, 20000, 1000),
(19, 'TI.PJ.435.19', '2016-06-01', '07:14:19', 38000, 50000, 12000),
(20, 'TI.PJ.435.20', '2016-06-01', '23:44:10', 80000, 100000, 20000),
(21, 'TI.PJ.435.21', '2016-06-01', '23:52:27', 80000, 100000, 20000),
(22, 'TI.PJ.435.22', '2016-06-01', '23:54:45', 80000, 100000, 20000),
(23, 'TI.PJ.435.23', '2016-06-02', '08:51:40', 100000, 100000, 0),
(24, 'TI.PJ.435.24', '2016-06-03', '21:37:54', 38000, 40000, 2000),
(25, 'TI.PJ.435.25', '2016-06-13', '09:41:13', 240000, 300000, 60000),
(26, 'TI.PJ.435.26', '2017-03-19', '12:53:29', 2000000, 5000000, 3000000),
(27, 'TI.PJ.435.27', '2017-03-19', '13:09:50', 200000, 1000000, 800000);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_supplier`
--

CREATE TABLE `tabel_supplier` (
  `kode` varchar(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telp` varchar(25) NOT NULL,
  `fax` varchar(25) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_supplier`
--

INSERT INTO `tabel_supplier` (`kode`, `nama`, `alamat`, `telp`, `fax`, `email`) VALUES
('0002', 'PT. Pelajaran', 'Jakarta', '123456789', '123456789', 'pt.pelajaran@email.com'),
('0003', 'PT. Majalah', 'Depok', '02100222', '02100223', 'majalaj@email.com'),
('11224', 'Jamettehhh', 'kamphoeng zamedth', '098765432', '33456', 'jamet@jamet.jamet'),
('456', 'erlangga', 'jl.nakula 1 rt 02/08', '083874719239', '123', 'farizalfatah@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_user`
--

CREATE TABLE `tabel_user` (
  `user` varchar(20) NOT NULL,
  `pass` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tabel_user`
--

INSERT INTO `tabel_user` (`user`, `pass`) VALUES
('admin', 'admin'),
('admin1', 'admin'),
('admin2', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_barang`
--
ALTER TABLE `tabel_barang`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tabel_karyawan`
--
ALTER TABLE `tabel_karyawan`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tabel_penjualan`
--
ALTER TABLE `tabel_penjualan`
  ADD PRIMARY KEY (`no`),
  ADD UNIQUE KEY `no_transaksi` (`no_transaksi`);

--
-- Indexes for table `tabel_supplier`
--
ALTER TABLE `tabel_supplier`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tabel_user`
--
ALTER TABLE `tabel_user`
  ADD PRIMARY KEY (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_penjualan`
--
ALTER TABLE `tabel_penjualan`
  MODIFY `no` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
