/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import database.Koneksi;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rezayds
 */
public class FormBarang extends javax.swing.JInternalFrame {

    /**
     * Creates new form FormBarang
     */
    public FormBarang() {
        initComponents();
        fillDataToTable();
        hakAkses(true);
    }
    
    private void hakAkses(boolean akses){
        tombolSimpan.setEnabled(akses);
        tombolUbah.setEnabled(!akses);
        tombolHapus.setEnabled(!akses);
    }
    
    private void tambah(){
        textKodeSup.setText("");
        textNamaSup.setText("");
        textKodeBrg.setText("");
        textNamaBrg.setText("");
        textHargaBrg.setText("");
        textJumlahBrg.setText("");
        hakAkses(true);
    }
    
    private void simpan(){
        String query;
        query = "INSERT INTO tabel_barang (kode, nama_sup, nama, jenis, satuan, "
                + "harga, jumlah) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeBrg.getText());
            statement.setString(2, textNamaSup.getText());
            statement.setString(3, textNamaBrg.getText());
            statement.setString(4, comboJenis.getSelectedItem().toString());
            statement.setString(5, comboSatuan.getSelectedItem().toString());
            statement.setString(6, textHargaBrg.getText());
            statement.setString(7, textJumlahBrg.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data barang telah disimpan");
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void ubah(){
        String query;
        query = "UPDATE tabel_barang SET nama_sup = ?, nama = ?, jenis = ?, "
                + "satuan = ?, harga = ?, jumlah = ? WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textNamaSup.getText());
            statement.setString(2, textNamaBrg.getText());
            statement.setString(3, comboJenis.getSelectedItem().toString());
            statement.setString(4, comboSatuan.getSelectedItem().toString());
            statement.setString(5, textHargaBrg.getText());
            statement.setString(6, textJumlahBrg.getText());
            statement.setString(7, textKodeBrg.getText());
            
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data barang telah diubah");
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void hapus(){
        String query;
        query = "DELETE FROM tabel_barang WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeBrg.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data barang telah dihapus");
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void cari(){
        try{
            String pertanyaan = "Masukan kode barang yang dicari";
            String kode = JOptionPane.showInputDialog(this, pertanyaan);
            if(!kode.trim().equals("")){
                String query = "SELECT * FROM tabel_barang WHERE kode = ?";
                PreparedStatement statement;
                Connection connection;
                try{
                    connection = Koneksi.koneksiDatabase();
                    statement = connection.prepareStatement(query);
                    statement.setString(1, kode);
                    ResultSet rs = statement.executeQuery();
                    if(rs.next()){
                        textKodeBrg.setText(rs.getString("kode"));
                        textNamaSup.setText(rs.getString("nama_sup"));
                        textNamaBrg.setText(rs.getString("nama"));
                        comboJenis.setSelectedItem(rs.getString("jenis"));
                        comboSatuan.setSelectedItem(rs.getString("satuan"));
                        textHargaBrg.setText(rs.getString("harga"));
                        textJumlahBrg.setText(rs.getString("jumlah"));
                        hakAkses(false);
                    }else{
                        JOptionPane.showMessageDialog(this, "Data barang tidak ditemukan");
                        tambah();
                    }
                }catch(SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        }catch(HeadlessException e){
        }
    }
    
    private void cari_sup(){
        String query = "SELECT * FROM tabel_supplier WHERE kode = ?";
        PreparedStatement statement;
        Connection connection = Koneksi.koneksiDatabase();
        try{
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeSup.getText());
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                textNamaSup.setText(rs.getString("nama"));
                textKodeSup.requestFocus();
            }else{
                JOptionPane.showMessageDialog(this, "Data supplier tidak ada");
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void fillDataToTable(){
        DefaultTableModel tabelModel;
        tabelModel = (DefaultTableModel) tabelBarang.getModel();
        String query = "SELECT * FROM tabel_barang";
        Connection connection;
        Statement statement;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet hasil = statement.executeQuery(query);
            while(hasil.next()){
                String kode = hasil.getString("kode");
                String nama_sup = hasil.getString("nama_sup");
                String nama = hasil.getString("nama");
                String jenis = hasil.getString("jenis");
                String harga = hasil.getString("harga");
                String jumlah = hasil.getString("jumlah");
                String satuan = hasil.getString("satuan");
                Object[] data = new Object[]{kode, nama_sup, nama, jenis, harga,
                    jumlah, satuan};
                tabelModel.addRow(data);
            }
            statement.close();
        }catch(SQLException e){
            String pesan = "Telah terjadi kesalahan pada proses isi tabel ";
            JOptionPane.showMessageDialog(this, pesan + e.getMessage());
        }
    }
    
    private void refreshDataInTable(){
        DefaultTableModel tabelModel;
        tabelModel = (DefaultTableModel) tabelBarang.getModel();
        int jumlahBaris = tabelBarang.getRowCount();
        for(int i = 0; i < jumlahBaris; i++){
            tabelModel.removeRow(0);
        }
        fillDataToTable();
    }    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        textKodeBrg = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        textNamaBrg = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        textHargaBrg = new javax.swing.JTextField();
        textJumlahBrg = new javax.swing.JTextField();
        comboJenis = new javax.swing.JComboBox();
        comboSatuan = new javax.swing.JComboBox();
        tombolTambah = new javax.swing.JButton();
        tombolSimpan = new javax.swing.JButton();
        tombolUbah = new javax.swing.JButton();
        tombolHapus = new javax.swing.JButton();
        tombolCari = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelBarang = new javax.swing.JTable();
        tombolRefresh = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        textKodeSup = new javax.swing.JTextField();
        tombolCariSup = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        textNamaSup = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();

        setClosable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Input Data Buku");

        jLabel2.setText("Kode Buku");

        jLabel3.setText("Judul Buku");

        jLabel4.setText("Jenis");

        jLabel5.setText("Satuan");

        jLabel6.setText("Harga");

        jLabel7.setText("Jumlah");

        textHargaBrg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textHargaBrgActionPerformed(evt);
            }
        });

        comboJenis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Novel", "Komik", "Majalah", "Pelajaran" }));

        comboSatuan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pcs", "Eksemplar" }));

        tombolTambah.setText("Tambah");
        tombolTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTambahActionPerformed(evt);
            }
        });

        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        tombolUbah.setText("Ubah");
        tombolUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolUbahActionPerformed(evt);
            }
        });

        tombolHapus.setText("Hapus");
        tombolHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHapusActionPerformed(evt);
            }
        });

        tombolCari.setText("Cari");
        tombolCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolCariActionPerformed(evt);
            }
        });

        tabelBarang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Kode", "Nama Supplier", "Judul Buku", "Jenis", "Harga", "Jumlah", "Satuan"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabelBarang);

        tombolRefresh.setText("Refresh Tabel");
        tombolRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolRefreshActionPerformed(evt);
            }
        });

        jLabel8.setText("Kode Supplier");

        tombolCariSup.setText("Cari");
        tombolCariSup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolCariSupActionPerformed(evt);
            }
        });

        jLabel9.setText("Nama Supplier");

        textNamaSup.setEditable(false);

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/buku-input.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(28, 28, 28)
                        .addComponent(textKodeSup, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tombolCariSup)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tombolTambah)
                        .addGap(18, 18, 18)
                        .addComponent(tombolSimpan)
                        .addGap(18, 18, 18)
                        .addComponent(tombolUbah)
                        .addGap(18, 18, 18)
                        .addComponent(tombolHapus)
                        .addGap(18, 18, 18)
                        .addComponent(tombolCari)
                        .addGap(18, 18, 18)
                        .addComponent(tombolRefresh)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7))
                                .addGap(25, 25, 25)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textNamaSup, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textKodeBrg, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textNamaBrg, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comboJenis, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(comboSatuan, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textHargaBrg, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textJumlahBrg, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(33, 33, 33))
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(101, 101, 101))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(textKodeSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tombolCariSup))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(textNamaSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(textKodeBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(textNamaBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(comboJenis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(comboSatuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textHargaBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(textJumlahBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolTambah)
                    .addComponent(tombolSimpan)
                    .addComponent(tombolUbah)
                    .addComponent(tombolHapus)
                    .addComponent(tombolCari)
                    .addComponent(tombolRefresh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void textHargaBrgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textHargaBrgActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textHargaBrgActionPerformed

    private void tombolTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTambahActionPerformed
        tambah();
    }//GEN-LAST:event_tombolTambahActionPerformed

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
        simpan();
        refreshDataInTable();
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolUbahActionPerformed
        ubah();
        refreshDataInTable();
    }//GEN-LAST:event_tombolUbahActionPerformed

    private void tombolHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHapusActionPerformed
        hapus();
        refreshDataInTable();
    }//GEN-LAST:event_tombolHapusActionPerformed

    private void tombolCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolCariActionPerformed
        cari();
    }//GEN-LAST:event_tombolCariActionPerformed

    private void tombolRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolRefreshActionPerformed
        refreshDataInTable();
    }//GEN-LAST:event_tombolRefreshActionPerformed

    private void tombolCariSupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolCariSupActionPerformed
        cari_sup();
    }//GEN-LAST:event_tombolCariSupActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox comboJenis;
    private javax.swing.JComboBox comboSatuan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelBarang;
    private javax.swing.JTextField textHargaBrg;
    private javax.swing.JTextField textJumlahBrg;
    private javax.swing.JTextField textKodeBrg;
    private javax.swing.JTextField textKodeSup;
    private javax.swing.JTextField textNamaBrg;
    private javax.swing.JTextField textNamaSup;
    private javax.swing.JButton tombolCari;
    private javax.swing.JButton tombolCariSup;
    private javax.swing.JButton tombolHapus;
    private javax.swing.JButton tombolRefresh;
    private javax.swing.JButton tombolSimpan;
    private javax.swing.JButton tombolTambah;
    private javax.swing.JButton tombolUbah;
    // End of variables declaration//GEN-END:variables
}
