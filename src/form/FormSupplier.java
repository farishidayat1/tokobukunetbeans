/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import database.Koneksi;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rezayds
 */
public class FormSupplier extends javax.swing.JInternalFrame {
    
     private void hakAkses(boolean akses){
        tombolSimpan.setEnabled(akses);
        tombolUbah.setEnabled(!akses);
        tombolHapus.setEnabled(!akses);
    }
     
    private void tambah(){
        textKodeSup.setText("");
        textNamaSup.setText("");
        textAlamatSup.setText("");
        textTelpSup.setText("");
        textFaxSup.setText("");
        textEmailSup.setText("");
        hakAkses(true);
    }
     
    private void simpan(){
        String query;
        query = "INSERT INTO tabel_supplier (kode, nama, alamat, telp,"
                + " fax, email) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeSup.getText());
            statement.setString(2, textNamaSup.getText());
            statement.setString(3, textAlamatSup.getText());
            statement.setString(4, textTelpSup.getText());
            statement.setString(5, textFaxSup.getText());
            statement.setString(6, textEmailSup.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data supplier telah disimpan");
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void ubah(){
        String query;
        query = "UPDATE tabel_supplier SET nama = ?, alamat = ?, telp = ?, "
                + "fax = ?, email = ? WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);          
            statement.setString(1, textNamaSup.getText());
            statement.setString(2, textAlamatSup.getText());
            statement.setString(3, textTelpSup.getText());
            statement.setString(4, textFaxSup.getText());
            statement.setString(5, textEmailSup.getText());
            statement.setString(6, textKodeSup.getText());
            
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data supplier telah diubah");
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void hapus(){
        String query;
        query = "DELETE FROM tabel_supplier WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeSup.getText());
            
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data supplier telah dihapus");
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
     private void cari(String kode){
        String query;
        query = "SELECT * FROM tabel_supplier WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, kode);
            ResultSet hasil = statement.executeQuery();
            if(hasil.next()){
                textKodeSup.setText(hasil.getString("kode"));
                textNamaSup.setText(hasil.getString("nama"));
                textAlamatSup.setText(hasil.getString("alamat"));
                textTelpSup.setText(hasil.getString("telp"));
                textFaxSup.setText(hasil.getString("fax"));
                textEmailSup.setText(hasil.getString("email"));
                hakAkses(false);
            }else{
                String pesan = "Data supplier tidak ditemukan";
                JOptionPane.showMessageDialog(this, pesan);
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
     
     private void fillDataToTable(){
        DefaultTableModel tabelModel;
        tabelModel = (DefaultTableModel) tabelSupplier.getModel();
        String query = "SELECT * FROM tabel_supplier";
        Connection connection;
        Statement statement;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet hasil = statement.executeQuery(query);
            while(hasil.next()){
                String kode = hasil.getString("kode");
                String nama = hasil.getString("nama");
                String alamat = hasil.getString("alamat");
                String telp = hasil.getString("telp");
                String email = hasil.getString("email");
                Object[] data = new Object[]{kode, nama, alamat, telp, email};
                tabelModel.addRow(data);
            }
            statement.close();
        }catch(SQLException e){
            String pesan = "Telah terjadi kesalahan pada proses isi tabel ";
            JOptionPane.showMessageDialog(this, pesan + e.getMessage());
        }
    }
    
    private void refreshDataInTable(){
        DefaultTableModel tabelModel;
        tabelModel = (DefaultTableModel) tabelSupplier.getModel();
        int jumlahBaris = tabelSupplier.getRowCount();
        for(int i = 0; i < jumlahBaris; i++){
            tabelModel.removeRow(0);
        }
        fillDataToTable();
    }

    /**
     * Creates new form FormSupplier
     */
    public FormSupplier() {
        initComponents();
        hakAkses(true);
        fillDataToTable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textKodeSup = new javax.swing.JTextField();
        textNamaSup = new javax.swing.JTextField();
        textTelpSup = new javax.swing.JTextField();
        textFaxSup = new javax.swing.JTextField();
        textEmailSup = new javax.swing.JTextField();
        tombolTambah = new javax.swing.JButton();
        tombolSimpan = new javax.swing.JButton();
        tombolUbah = new javax.swing.JButton();
        tombolHapus = new javax.swing.JButton();
        tombolCari = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tombolRefresh = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelSupplier = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        textAlamatSup = new javax.swing.JTextArea();

        setClosable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Input Data Supplier");

        jLabel2.setText("Nama Supplier");

        jLabel3.setText("Kode Supplier");

        tombolTambah.setText("Tambah");
        tombolTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTambahActionPerformed(evt);
            }
        });

        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        tombolUbah.setText("Ubah");
        tombolUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolUbahActionPerformed(evt);
            }
        });

        tombolHapus.setText("Hapus");
        tombolHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHapusActionPerformed(evt);
            }
        });

        tombolCari.setText("Cari");
        tombolCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolCariActionPerformed(evt);
            }
        });

        jLabel4.setText("Alamat");

        jLabel5.setText("Telp");

        jLabel6.setText("Fax");

        jLabel7.setText("Email");

        tombolRefresh.setText("Refresh Tabel");
        tombolRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolRefreshActionPerformed(evt);
            }
        });

        tabelSupplier.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Kode", "Nama", "Alamat", "Telp", "Email"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabelSupplier);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/supplier-input.png"))); // NOI18N

        textAlamatSup.setColumns(20);
        textAlamatSup.setRows(5);
        jScrollPane2.setViewportView(textAlamatSup);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tombolTambah)
                                .addGap(18, 18, 18)
                                .addComponent(tombolSimpan)
                                .addGap(18, 18, 18)
                                .addComponent(tombolUbah)
                                .addGap(18, 18, 18)
                                .addComponent(tombolHapus)
                                .addGap(18, 18, 18)
                                .addComponent(tombolCari)
                                .addGap(18, 18, 18)
                                .addComponent(tombolRefresh))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel5))
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textEmailSup, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(textFaxSup, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(textTelpSup, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(textKodeSup, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(textNamaSup, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                                        .addComponent(jScrollPane2)))))
                        .addGap(0, 259, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(119, 119, 119))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(89, 89, 89))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(textKodeSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textNamaSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textTelpSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textFaxSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(textEmailSup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolTambah)
                    .addComponent(tombolSimpan)
                    .addComponent(tombolUbah)
                    .addComponent(tombolHapus)
                    .addComponent(tombolCari)
                    .addComponent(tombolRefresh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tombolTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTambahActionPerformed
        tambah();
    }//GEN-LAST:event_tombolTambahActionPerformed

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
        if(textKodeSup.getText().trim().equals("")){
            String pesan = "Kode supplier tidak boleh kosong";
            JOptionPane.showMessageDialog(this, pesan);
        }else{
            simpan();
            refreshDataInTable();
        }
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolUbahActionPerformed
        try{
            String pesan = "Apakah anda yakin ingin merubah data ini?";
            String judul = "Konfirmasi Perubahan";
            int reply = JOptionPane.showConfirmDialog(this, pesan, judul, 0, 3);
            if(reply == JOptionPane.YES_OPTION){
                ubah();
                refreshDataInTable();
            }
        }catch(HeadlessException e){
            
        }
    }//GEN-LAST:event_tombolUbahActionPerformed

    private void tombolHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHapusActionPerformed
        try{
            String pesan = "Apakah anda yakin ingin menghapus data ini?";
            String judul = "Konfirmasi Penghapusan";
            int reply = JOptionPane.showConfirmDialog(this, pesan, judul, 0, 3);
            if(reply == JOptionPane.YES_OPTION){
                hapus();
                refreshDataInTable();
            }
        }catch(HeadlessException e){
            
        }
    }//GEN-LAST:event_tombolHapusActionPerformed

    private void tombolCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolCariActionPerformed
        try{
            String pesan = "Masukan kode supplier yang ingin anda cari :";
            String kode = JOptionPane.showInputDialog(this, pesan);
            if(!kode.trim().equals("")){
                cari(kode);
            }
        }catch(NullPointerException e){
            
        }
    }//GEN-LAST:event_tombolCariActionPerformed

    private void tombolRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolRefreshActionPerformed
        refreshDataInTable();
    }//GEN-LAST:event_tombolRefreshActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabelSupplier;
    private javax.swing.JTextArea textAlamatSup;
    private javax.swing.JTextField textEmailSup;
    private javax.swing.JTextField textFaxSup;
    private javax.swing.JTextField textKodeSup;
    private javax.swing.JTextField textNamaSup;
    private javax.swing.JTextField textTelpSup;
    private javax.swing.JButton tombolCari;
    private javax.swing.JButton tombolHapus;
    private javax.swing.JButton tombolRefresh;
    private javax.swing.JButton tombolSimpan;
    private javax.swing.JButton tombolTambah;
    private javax.swing.JButton tombolUbah;
    // End of variables declaration//GEN-END:variables
}
