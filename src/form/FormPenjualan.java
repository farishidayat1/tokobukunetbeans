/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import com.sun.glass.events.KeyEvent;
import database.Koneksi;
import java.awt.Toolkit;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author rezayds
 */
public class FormPenjualan extends javax.swing.JInternalFrame {

    private String getNoTransaksi(){
        String noTransaksi = "";
        String query;
        query = "SELECT MAX(no) AS no FROM tabel_penjualan";
        Connection connection;
        Statement statement;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if (rs.next()){
                int noTerbesar = rs.getInt("no") + 1;
                noTransaksi = "TI.PJ.435." + noTerbesar;
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return noTransaksi;
    }
    
    private void bersihDataBarang(){
        textKodeBrg.setText("");
        textNamaBrg.setText("");
        textHargaBrg.setText("");
        textKodeBrg.requestFocus();
    }
    
    private void refreshTransaksi(){
        bersihDataBarang();
        DefaultTableModel tableModel;
        tableModel = (DefaultTableModel) tabelPenjualan.getModel();
        int jumlahBaris = tabelPenjualan.getRowCount();
        for (int i = 0; i < jumlahBaris; i++){
            tableModel.removeRow(0);
        }
        textTotal.setText("");
        textUangBayar.setText("");
        textUangKembali.setText("");
        textNoTransaksi.setText(getNoTransaksi());
        textKodeBrg.requestFocus();
    }
    
    private void cari(){
        String query = "SELECT * FROM tabel_barang WHERE kode = ?";
        PreparedStatement statement;
        Connection connection = Koneksi.koneksiDatabase();
        try{
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeBrg.getText());
            ResultSet rs = statement.executeQuery();
            if(rs.next()){
                textKodeBrg.setText(rs.getString("kode"));
                textNamaBrg.setText(rs.getString("nama"));
                textHargaBrg.setText(rs.getString("harga"));
                textJumlahBrg.requestFocus();
            }else{
                JOptionPane.showMessageDialog(this, "Data barang tidak ada");
                bersihDataBarang();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        
    }
    
    private void isiTabelPenjualan(){
        DefaultTableModel tableModel;
        tableModel = (DefaultTableModel) tabelPenjualan.getModel();
        String kode = textKodeBrg.getText();
        String nama = textNamaBrg.getText();
        int harga = Integer.parseInt(textHargaBrg.getText());
        int jumlah = Integer.parseInt(textJumlahBrg.getText());
        int total = harga*jumlah;
        Object[] data = new Object[]{kode, nama, harga, jumlah, total};
        tableModel.addRow(data);
    }
    
    private void hapusBarangPenjualan(){
        DefaultTableModel tableModel;
        tableModel = (DefaultTableModel) tabelPenjualan.getModel();
        int pilihBaris = tabelPenjualan.getSelectedRow();
        tableModel.removeRow(pilihBaris);
    }
    
    private void hitungTotalBelanja(){
        int jumlahBelanja = tabelPenjualan.getRowCount();
        int total = 0;
        if(jumlahBelanja > 0){
            for (int i = 0; i < jumlahBelanja; i++){
                String totalDiTabel = tabelPenjualan.getValueAt(i, 4).toString();
                total += Integer.parseInt(totalDiTabel);
            }
            textTotal.setText(String.valueOf(total));
        }
    }
    
    private void hitungUangKembali(){
        int total = Integer.parseInt(textTotal.getText());
        int uangBayar = Integer.parseInt(textUangBayar.getText());
        int kembali = uangBayar - total;
        textUangKembali.setText(String.valueOf(kembali));
    }
    
    private void simpanPenjualan(){
        String query;
        query = "INSERT INTO tabel_penjualan (no_transaksi, tanggal, waktu, "
                + "total_belanja, uang_bayar, uang_kembali) VALUES (?,now(),now(),?,?,?)";
        PreparedStatement statement;
        Connection connection = Koneksi.koneksiDatabase();
        try {
            connection.setAutoCommit(false);
            statement = connection.prepareStatement(query);
            statement.setString(1, textNoTransaksi.getText());
            statement.setInt(2, Integer.parseInt(textTotal.getText()));
            statement.setInt(3, Integer.parseInt(textUangBayar.getText()));
            statement.setInt(4, Integer.parseInt(textUangKembali.getText()));
            int hasil = statement.executeUpdate();
            if (hasil == 1) {
                String queryDetail;
                queryDetail = "INSERT INTO tabel_detail_penjualan (no_transaksi, kode_brg, "
                        + " tanggal, nama, harga, jumlah, total) VALUES (?,?,now(),?,?,?,?)";
                int jumlahBarang = tabelPenjualan.getRowCount();
                for (int i = 0; i < jumlahBarang; i++) {
                    statement = connection.prepareStatement(queryDetail);
                    statement.setString(1, textNoTransaksi.getText());
                    statement.setString(2, tabelPenjualan.getValueAt(i, 0).toString());
                    statement.setString(3, tabelPenjualan.getValueAt(i, 1).toString());
                    statement.setString(4, tabelPenjualan.getValueAt(i, 2).toString());
                    statement.setString(5, tabelPenjualan.getValueAt(i, 3).toString());
                    statement.setString(6, tabelPenjualan.getValueAt(i, 4).toString());
                    statement.executeUpdate();
                }
            }
            connection.commit();
            JOptionPane.showMessageDialog(this, "Transaksi Selesai");
        } catch (NumberFormatException | SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
            }
            JOptionPane.showMessageDialog(this, "ERROR : " + e.getMessage());
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
            }
        }
    }
    
    /**
     * Creates new form FormPenjualan
     */
    public FormPenjualan() {
        initComponents();
        Date now = new Date();
        SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");
        this.textTanggal.setText(sf.format(now));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        textNoTransaksi = new javax.swing.JTextField();
        textTanggal = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textKodeBrg = new javax.swing.JTextField();
        tombolCari = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        textHargaBrg = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        textNamaBrg = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        textJumlahBrg = new javax.swing.JTextField();
        tombolTambah = new javax.swing.JButton();
        tombolKurang = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelPenjualan = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        textTotal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        textUangBayar = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        textUangKembali = new javax.swing.JTextField();
        tombolTransaksi = new javax.swing.JButton();
        tombolSimpan = new javax.swing.JButton();
        tombolCetak = new javax.swing.JButton();

        setClosable(true);

        jLabel1.setText("No Transaksi");

        textNoTransaksi.setEditable(false);

        jLabel2.setText("Tanggal");

        jLabel3.setText("Kode Barang");

        textKodeBrg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textKodeBrgKeyTyped(evt);
            }
        });

        tombolCari.setText("Cari");
        tombolCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolCariActionPerformed(evt);
            }
        });

        jLabel4.setText("Harga");

        textHargaBrg.setEditable(false);

        jLabel5.setText("Nama Barang");

        textNamaBrg.setEditable(false);

        jLabel6.setText("Jumlah");

        textJumlahBrg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textJumlahBrgKeyTyped(evt);
            }
        });

        tombolTambah.setText("+");
        tombolTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTambahActionPerformed(evt);
            }
        });

        tombolKurang.setText("-");
        tombolKurang.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolKurangActionPerformed(evt);
            }
        });

        tabelPenjualan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Kode", "Nama", "Harga", "Jumlah", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelPenjualan.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tabelPenjualan);
        if (tabelPenjualan.getColumnModel().getColumnCount() > 0) {
            tabelPenjualan.getColumnModel().getColumn(3).setResizable(false);
            tabelPenjualan.getColumnModel().getColumn(4).setResizable(false);
        }

        jLabel7.setText("Total");

        textTotal.setEditable(false);

        jLabel8.setText("Uang Bayar");

        textUangBayar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textUangBayarKeyTyped(evt);
            }
        });

        jLabel9.setText("Kembali");

        textUangKembali.setEditable(false);

        tombolTransaksi.setText("Transaksi Baru");
        tombolTransaksi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTransaksiActionPerformed(evt);
            }
        });

        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        tombolCetak.setText("Cetak");
        tombolCetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolCetakActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(textNoTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(textTanggal, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(textKodeBrg, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tombolCari))
                            .addComponent(textHargaBrg))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textNamaBrg)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(textJumlahBrg, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(textTotal)
                            .addComponent(textUangBayar)
                            .addComponent(textUangKembali, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tombolTambah)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tombolKurang))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tombolTransaksi)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tombolSimpan)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tombolCetak)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(textNoTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textTanggal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(textKodeBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tombolCari)
                    .addComponent(jLabel5)
                    .addComponent(textNamaBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(textHargaBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(textJumlahBrg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolTambah)
                    .addComponent(tombolKurang))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(textTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(textUangBayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(textUangKembali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolTransaksi)
                    .addComponent(tombolSimpan)
                    .addComponent(tombolCetak))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tombolCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolCariActionPerformed
        cari();
    }//GEN-LAST:event_tombolCariActionPerformed

    private void textKodeBrgKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textKodeBrgKeyTyped
        if(evt.getKeyChar() == KeyEvent.VK_ENTER){
            cari();
        }
    }//GEN-LAST:event_textKodeBrgKeyTyped

    private void textUangBayarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textUangBayarKeyTyped
        if(evt.getKeyChar() == KeyEvent.VK_ENTER){
            hitungUangKembali();
        }else if(!Character.isDigit(evt.getKeyChar())){
            evt.consume();
            Toolkit.getDefaultToolkit().beep();
        }
    }//GEN-LAST:event_textUangBayarKeyTyped

    private void tombolTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTambahActionPerformed
        if(textNamaBrg.getText().trim().equals("")){
            JOptionPane.showMessageDialog(this, "Data barang belum lengkap");
        } else if(textJumlahBrg.getText().trim().equals("")){
            JOptionPane.showMessageDialog(this, "Jumlah masih kosong");
        } else {
            isiTabelPenjualan();
            bersihDataBarang();
            hitungTotalBelanja();
        }
    }//GEN-LAST:event_tombolTambahActionPerformed

    private void tombolKurangActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolKurangActionPerformed
        int baris = tabelPenjualan.getSelectedRowCount();
        if(baris == 1){
            hapusBarangPenjualan();
        }else{
            String pesan = "Silahkan pilih barang yang ingin anda hapus\n";
            pesan += "pada tabel transaksi penjualan";
            JOptionPane.showMessageDialog(this, pesan);
        }
    }//GEN-LAST:event_tombolKurangActionPerformed

    private void textJumlahBrgKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textJumlahBrgKeyTyped
        if(evt.getKeyChar() == KeyEvent.VK_ENTER){
            if(textNamaBrg.getText().trim().equals("")){
                JOptionPane.showMessageDialog(this, "Data barang belum lengkap");
            }else if(textJumlahBrg.getText().trim().equals("")){
                JOptionPane.showMessageDialog(this, "Jumlah barang masih kosong");
            }else{
                isiTabelPenjualan();
                bersihDataBarang();
                hitungTotalBelanja();
            }
        }
    }//GEN-LAST:event_textJumlahBrgKeyTyped

    private void tombolTransaksiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTransaksiActionPerformed
        refreshTransaksi();
    }//GEN-LAST:event_tombolTransaksiActionPerformed

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
        if(tabelPenjualan.getRowCount() < 1){
            String pesan = "Kesalahan dalam transaksi\n";
            pesan += "Tidak ada barang dalam tabel penjualan";
            JOptionPane.showMessageDialog(this, pesan);
        }else if(textUangBayar.getText().trim().equals("")){
            String pesan = "Kesalahan dalam transaksi\n";
            pesan += "Anda belum memasukan uang bayar";
            JOptionPane.showMessageDialog(this, pesan);
        }else if(textUangKembali.getText().trim().equals("")){
            String pesan = "Kesalahan dalam transaksi\n";
            pesan += "Anda belum menghitung total belanja";
            JOptionPane.showMessageDialog(this, pesan);
        }else{
            simpanPenjualan();
        }
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolCetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolCetakActionPerformed
        InputStream streamLaporan;
        streamLaporan = getClass().getResourceAsStream
        ("/laporan/laporan_penjualan_satuan.jasper");
        try{            
            HashMap hash = new HashMap();
            hash.put("no_transaksi", textNoTransaksi.getText());
            
            JasperPrint laporan = 
                    JasperFillManager.fillReport(streamLaporan, hash, Koneksi.koneksiDatabase());
            JasperViewer.viewReport(laporan, false);
        }catch(JRException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }//GEN-LAST:event_tombolCetakActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelPenjualan;
    private javax.swing.JTextField textHargaBrg;
    private javax.swing.JTextField textJumlahBrg;
    private javax.swing.JTextField textKodeBrg;
    private javax.swing.JTextField textNamaBrg;
    private javax.swing.JTextField textNoTransaksi;
    private javax.swing.JTextField textTanggal;
    private javax.swing.JTextField textTotal;
    private javax.swing.JTextField textUangBayar;
    private javax.swing.JTextField textUangKembali;
    private javax.swing.JButton tombolCari;
    private javax.swing.JButton tombolCetak;
    private javax.swing.JButton tombolKurang;
    private javax.swing.JButton tombolSimpan;
    private javax.swing.JButton tombolTambah;
    private javax.swing.JButton tombolTransaksi;
    // End of variables declaration//GEN-END:variables
}
