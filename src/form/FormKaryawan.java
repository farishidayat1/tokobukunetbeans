/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import database.Koneksi;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author rezayds
 */
public class FormKaryawan extends javax.swing.JInternalFrame {
    
    private void hakAkses(boolean akses){
        tombolSimpan.setEnabled(akses);
        tombolUbah.setEnabled(!akses);
        tombolHapus.setEnabled(!akses);
    }
    
    private void tambah(){
        textKodeKar.setText("");
        textNamaKar.setText("");
        textTempatLhr.setText("");
        textTanggalLhr.setDate(null);
        textAlamat.setText("");
        textTelp.setText("");
        hakAkses(true);
    }
    
    private void simpan(){
        String query;
        query = "INSERT INTO tabel_karyawan (kode, nama, kelamin, tempat_lahir,"
                + " tanggal_lahir, alamat, telp) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement;
        Connection connection;
        java.sql.Date tgl = new java.sql.Date(textTanggalLhr.getDate().getTime());
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeKar.getText());
            statement.setString(2, textNamaKar.getText());
            statement.setString(3, comboKelamin.getSelectedItem().toString());
            statement.setString(4, textTempatLhr.getText());
            statement.setDate(5, tgl);
            statement.setString(6, textAlamat.getText());
            statement.setString(7, textTelp.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data karyawan telah disimpan");
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void ubah(){
        String query;
        query = "UPDATE tabel_karyawan SET nama = ?, kelamin = ?, "
                + "tempat_lahir = ?, tanggal_lahir = ?, alamat = ?, telp = ?"
                + "WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        java.sql.Date tgl = new java.sql.Date(textTanggalLhr.getDate().getTime());
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);          
            statement.setString(1, textNamaKar.getText());
            statement.setString(2, comboKelamin.getSelectedItem().toString());
            statement.setString(3, textTempatLhr.getText());
            statement.setDate(4, tgl);
            statement.setString(5, textAlamat.getText());
            statement.setString(6, textTelp.getText());
            statement.setString(7, textKodeKar.getText());
            
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data karyawan telah diubah");
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void hapus(){
        String query;
        query = "DELETE FROM tabel_karyawan WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textKodeKar.getText());
            
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data karyawan telah dihapus");
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void cari(String kode){
        String query;
        query = "SELECT * FROM tabel_karyawan WHERE kode = ?";
        PreparedStatement statement;
        Connection connection;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, kode);
            ResultSet hasil = statement.executeQuery();
            if(hasil.next()){
                textKodeKar.setText(hasil.getString("kode"));
                textNamaKar.setText(hasil.getString("nama"));
                comboKelamin.setSelectedItem(hasil.getString("kelamin"));
                textTempatLhr.setText(hasil.getString("tempat_lahir"));
                textTanggalLhr.setDate(hasil.getDate("tanggal_lahir"));
                textAlamat.setText(hasil.getString("alamat"));
                textTelp.setText(hasil.getString("telp"));
                hakAkses(false);
            }else{
                String pesan = "Data karyawan tidak ditemukan";
                JOptionPane.showMessageDialog(this, pesan);
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
    
    private void fillDataToTable(){
        DefaultTableModel tabelModel;
        tabelModel = (DefaultTableModel) tabelKaryawan.getModel();
        String query = "SELECT * FROM tabel_karyawan";
        Connection connection;
        Statement statement;
        try{
            connection = Koneksi.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet hasil = statement.executeQuery(query);
            while(hasil.next()){
                String kode = hasil.getString("kode");
                String nama = hasil.getString("nama");
                String kelamin = hasil.getString("kelamin");
                String tempat = hasil.getString("tempat_lahir");
                String tanggal = hasil.getString("tanggal_lahir");
                String alamat = hasil.getString("alamat");
                String telp = hasil.getString("telp");
                Object[] data = new Object[]{kode, nama, kelamin, 
                    tempat, tanggal, alamat, telp};
                tabelModel.addRow(data);
            }
            statement.close();
        }catch(SQLException e){
            String pesan = "Telah terjadi kesalahan pada proses isi tabel ";
            JOptionPane.showMessageDialog(this, pesan + e.getMessage());
        }
    }
    
    private void refreshDataInTable(){
        DefaultTableModel tabelModel;
        tabelModel = (DefaultTableModel) tabelKaryawan.getModel();
        int jumlahBaris = tabelKaryawan.getRowCount();
        for(int i = 0; i < jumlahBaris; i++){
            tabelModel.removeRow(0);
        }
        fillDataToTable();
    }

    /**
     * Creates new form FormKaryawan
     */
    public FormKaryawan() {
        initComponents();
        hakAkses(true);
        fillDataToTable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        textKodeKar = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        textNamaKar = new javax.swing.JTextField();
        comboKelamin = new javax.swing.JComboBox();
        textTempatLhr = new javax.swing.JTextField();
        textTanggalLhr = new com.toedter.calendar.JDateChooser();
        textTelp = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tombolTambah = new javax.swing.JButton();
        tombolSimpan = new javax.swing.JButton();
        tombolUbah = new javax.swing.JButton();
        tombolHapus = new javax.swing.JButton();
        tombolCari = new javax.swing.JButton();
        tombolRefresh = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelKaryawan = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        textAlamat = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();

        setClosable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Input Data Karyawan");

        jLabel2.setText("Kode Karyawan");

        comboKelamin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Laki-laki", "Perempuan" }));

        jLabel3.setText("Nama");

        jLabel4.setText("Jenis Kelamin");

        jLabel5.setText("Tempat Tgl Lahir");

        jLabel6.setText("Alamat");

        jLabel7.setText("Telp");

        tombolTambah.setText("Tambah");
        tombolTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolTambahActionPerformed(evt);
            }
        });

        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        tombolUbah.setText("Ubah");
        tombolUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolUbahActionPerformed(evt);
            }
        });

        tombolHapus.setText("Hapus");
        tombolHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHapusActionPerformed(evt);
            }
        });

        tombolCari.setText("Cari");
        tombolCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolCariActionPerformed(evt);
            }
        });

        tombolRefresh.setText("Refresh Tabel");
        tombolRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolRefreshActionPerformed(evt);
            }
        });

        tabelKaryawan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Kode", "Nama", "Jenis Kelamin", "Tempat Lahir", "Tanggal Lahir", "Alamat", "Telp"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabelKaryawan);

        textAlamat.setColumns(20);
        textAlamat.setRows(5);
        jScrollPane2.setViewportView(textAlamat);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/karyawan-input.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(comboKelamin, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(textNamaKar, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(textKodeKar, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(53, 53, 53)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(textTelp, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(textTempatLhr, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(textTanggalLhr, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addGap(83, 83, 83))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(57, 57, 57))))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tombolTambah)
                        .addGap(18, 18, 18)
                        .addComponent(tombolSimpan)
                        .addGap(18, 18, 18)
                        .addComponent(tombolUbah)
                        .addGap(18, 18, 18)
                        .addComponent(tombolHapus)
                        .addGap(18, 18, 18)
                        .addComponent(tombolCari)
                        .addGap(18, 18, 18)
                        .addComponent(tombolRefresh)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textKodeKar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(textNamaKar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(comboKelamin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(textTempatLhr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textTanggalLhr, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textTelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tombolTambah)
                    .addComponent(tombolSimpan)
                    .addComponent(tombolUbah)
                    .addComponent(tombolHapus)
                    .addComponent(tombolCari)
                    .addComponent(tombolRefresh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tombolTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolTambahActionPerformed
        tambah();
    }//GEN-LAST:event_tombolTambahActionPerformed

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
        if(textKodeKar.getText().trim().equals("")){
            String pesan = "Kode karyawan tidak boleh kosong";
            JOptionPane.showMessageDialog(this, pesan);
        }else{
            simpan();
            refreshDataInTable();
        }
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolUbahActionPerformed
        try{
            String pesan = "Apakah anda yakin ingin merubah data ini?";
            String judul = "Konfirmasi Perubahan";
            int reply = JOptionPane.showConfirmDialog(this, pesan, judul, 0, 3);
            if(reply == JOptionPane.YES_OPTION){
                ubah();
                refreshDataInTable();
            }
        }catch(HeadlessException e){
            
        }
    }//GEN-LAST:event_tombolUbahActionPerformed

    private void tombolHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHapusActionPerformed
        try{
            String pesan = "Apakah anda yakin ingin menghapus data ini?";
            String judul = "Konfirmasi Penghapusan";
            int reply = JOptionPane.showConfirmDialog(this, pesan, judul, 0, 3);
            if(reply == JOptionPane.YES_OPTION){
                hapus();
                refreshDataInTable();
            }
        }catch(HeadlessException e){
            
        }
    }//GEN-LAST:event_tombolHapusActionPerformed

    private void tombolCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolCariActionPerformed
        try{
            String pesan = "Masukan kode karyawan yang ingin anda cari :";
            String kode = JOptionPane.showInputDialog(this, pesan);
            if(!kode.trim().equals("")){
                cari(kode);
            }
        }catch(NullPointerException e){
            
        }
    }//GEN-LAST:event_tombolCariActionPerformed

    private void tombolRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolRefreshActionPerformed
        refreshDataInTable();
    }//GEN-LAST:event_tombolRefreshActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox comboKelamin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabelKaryawan;
    private javax.swing.JTextArea textAlamat;
    private javax.swing.JTextField textKodeKar;
    private javax.swing.JTextField textNamaKar;
    private com.toedter.calendar.JDateChooser textTanggalLhr;
    private javax.swing.JTextField textTelp;
    private javax.swing.JTextField textTempatLhr;
    private javax.swing.JButton tombolCari;
    private javax.swing.JButton tombolHapus;
    private javax.swing.JButton tombolRefresh;
    private javax.swing.JButton tombolSimpan;
    private javax.swing.JButton tombolTambah;
    private javax.swing.JButton tombolUbah;
    // End of variables declaration//GEN-END:variables
}
