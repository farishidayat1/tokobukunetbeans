/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author rezayds
 */
public class Koneksi {
    
    private static Connection koneksi;
    
    public static Connection koneksiDatabase(){
        if(koneksi == null){
            MysqlDataSource dataSource = new MysqlDataSource();
            dataSource.setURL("jdbc:mysql://localhost/db_tokobuku");
            dataSource.setUser("root");
            dataSource.setPassword("");
            try{
                koneksi = dataSource.getConnection();
            } catch(SQLException ex){
                JOptionPane.showMessageDialog(null, "KONEKSI ERROR :" + 
                        ex.getMessage());
            }
        }
        return koneksi;
    }
}
